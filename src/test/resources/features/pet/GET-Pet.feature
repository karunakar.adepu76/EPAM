Feature: Pet Store

  Information about the pet store available,sold,pending and Insert a new pet, update a pet and delete a pet.

  @Add
  Scenario Outline: Add available pets to the pets store
    Given I am an active user of petsstore
    When I add the available pets <id> <name> <status> to the store
    Then I should be able to see the newly added pet <id> in the store

    Examples:
      |id           |name        |status          |
      | 1223123666  |Gummy    |    available   |


  @Modify
  Scenario Outline: Modify a pet
    Given I am an active user of petsstore
    When I modify the name <name> of the pet with id <petId> from the petstore
    Then I should see the modified name <name> of the pet for the pet with petId <petId>
    Examples:
      |   name    |petId       |
      |   Jimmy   | 1223123666 |


  @GET
  Scenario Outline: Retrieve a pet information given by pet_Id and verify with its name
    Given I am an active user of petsstore
    When I request the pet from the store with <petId>
    Then I should see the name of the pet <name> for with petId <petId>

    Examples:
      |petId        |name |
      |1223123666   |Jimmy   |

  Scenario Outline: Retrieve all pets based on status(availability)
  based on the input, list of pets are returned.

    Given I am an active user of petsstore
    When I request the <status> pets from the store
    Then I should see all <status> pets from the store
    Examples:
      |status   |
      |available|
      |pending  |
      |sold     |

  @DELETE
  Scenario Outline: Delete a pet from PetStore
    Given I am an active user of petsstore
    When I place a request to delete the pet <petId>
    Then I should receive NotFounfd message when i search for the pet  <petId>
    Examples:
      |   petId      |
      |   1223123666 |