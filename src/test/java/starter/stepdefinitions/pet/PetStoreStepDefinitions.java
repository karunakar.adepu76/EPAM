package starter.stepdefinitions.pet;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.reports.AndContent;
import org.assertj.core.api.AbstractBooleanAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.StringAssert;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import org.junit.Assert;

import javax.swing.text.StringContent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;

public class PetStoreStepDefinitions {

    @Given("^I am an active user of petsstore$")
    public void i_am_an_active_user() {

        RestAssured.baseURI = "https://petstore.swagger.io/v2/pet";
    }

    //######################################################################################
    //@GET- By Status
    @When("^I request the (.*) pets from the store$")
    public void i_request_the_pets_from_the_store(String status) {
        when().get("/findByStatus?status={status}",status)
                .then().statusCode(200);
    }

    @Then("^I should see all (.*) pets from the store$")
    public void i_should_see_all_pets_from_the_store(String status) {
        JsonPath response = then().extract().response().jsonPath();
        ArrayList allids = response.get("id");
        StringBuilder builder = new StringBuilder();

        for (int i=0;i<allids.size();i++){

            builder.append(allids.get(i));
            builder.append("\n");
        }
        Serenity.recordReportData().withTitle("Id's's of the pets").andContents(builder.toString());
    }
    //######################################################################################
    //######################################################################################
    //@GET - By Id
    @When("^I request the pet from the store with (.*)$")
    public void iRequestThePetFromTheStoreWithPetid(String petId) {
        given().when().get("https://petstore.swagger.io/v2/pet/{id}",petId).then().
                statusCode(200);

    }

    @Then("^I should see the name of the pet (.*) for with petId (.*)$")
    public void iShouldSeeTheNameOfThePetNameWithPtId(String name, String petId) {
        when().get("/{id}",petId)
                .then().statusCode(200);
        JsonPath petNameResponse = then().extract().response().jsonPath();
        if (petNameResponse.get("name").toString().equalsIgnoreCase(name)){
            Serenity.recordReportData().withTitle("Retrieved Pet Name is").andContents(name.toString());
        }
    }

    //######################################################################################
    //######################################################################################
    //ADD-Pets
    @When("^I add the available pets (.*) (.*) (.*) to the store$")
    public void iAddTheAvailablePetsToTheStore(String id, String name, String status) {
        Map<String,String> pet = new HashMap<>();
        pet.put("id",id);
        pet.put("name", name);
        pet.put("status", status);
        given().urlEncodingEnabled(true)
                .contentType("application/json")
                .body(pet)
                .when().post("https://petstore.swagger.io/v2/pet").then()
                .statusCode(200);
    }

    @Then("^I should be able to see the newly added pet (.*) in the store$")
    public void iShouldBeAbleToSeeTheNewlyAddedPetInTheStore(String id) {
        when().get("/{id}",id)
                .then().statusCode(200);
        JsonPath response = then().extract().response().jsonPath();
        if (response.get("id").toString().equalsIgnoreCase(id)){
            //System.out.println("Hello");
            Serenity.recordReportData().withTitle("Newly added pets pet id is").andContents(id.toString());
        }
    }
    //######################################################################################
    //######################################################################################
    //PUT-Modify Pet
    @When("^I modify the name (.*) of the pet with id (.*) from the petstore$")
    public void iModifyTheNameOfThePetWithIdPetIdFromThePetstore(String name, String petId) {
        Map<String,String> pet = new HashMap<>();
        pet.put("id",petId);
        pet.put("name", name);
        given().urlEncodingEnabled(true)
                .contentType("application/json")
                .body(pet)
                .when().post("https://petstore.swagger.io/v2/pet").then()
                .statusCode(200);
    }

    @Then("^I should see the modified name (.*) of the pet for the pet with petId (.*)$")
    public void iShouldSeeTheModifiedNameOfThePetNameForThePetWithPetIdPetId(String name, String petId) {
        when().get("/{id}",petId)
                .then()
                .body("name",equalTo(name))
                .statusCode(200);
        JsonPath petNameModified = then().extract().response().jsonPath();
        if (petNameModified.get("name").toString().equalsIgnoreCase(name)){
//            System.out.println(("Hello"));
            Serenity.recordReportData().withTitle("Retrieved Pet Name is").andContents(name.toString());
        }
    }
    //######################################################################################
    //######################################################################################
    //DELETE-Pet

    @When("^I place a request to delete the pet (.*)$")
    public void iPlaceARequestToDeleteThePetPetId( String petId) {
        given()
                .header("api-key","special-key")
                .when().delete("https://petstore.swagger.io/v2/pet/{id}", petId)
                .then()
                .statusCode(200);


    }

    @Then("^I should receive NotFounfd message when i search for the pet (.*)$")
    public void iShouldReceiveNotFounfdMessageWhenISearchForThePet(String petId) {
        when().get("/{id}",petId)
                .then().statusCode(404);
//        JsonPath response = then().extract().response().jsonPath();
//        int statusCode;
//        statusCode = response.getStatusCode();
//        if(statusCode == 404) {
//            System.out.println(("Hello"));
//            Serenity.recordReportData().withTitle("Pet Deleted successfully ==>").andContents(petId.toString());
//        }

    }

    //######################################################################################

}
